//
//  LoadingCell.swift
//  moviesApp
//
//  Created by Apple on 13/01/19.
//  Copyright © 2019 sergiodev. All rights reserved.
//

import UIKit

class LoadingCell: UITableViewCell {
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
