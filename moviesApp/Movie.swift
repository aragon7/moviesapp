//
//  Movie.swift
//  moviesApp
//
//  Created by Apple on 7/01/19.
//  Copyright © 2019 sergiodev. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class Movie {
    let title: String
    let year: Int
    var rating: Int
    var overview: String?
    var tmdb_id: String
    var picture: UIImage?
    var imageRequest: DataRequest?
    
    init(json: JSON) {
        title = json["title"].stringValue
        year = json["year"].intValue
        rating = Int(round((json["rating"].doubleValue)*10))
        tmdb_id = json["ids"]["tmdb"].stringValue
    }
    
    deinit {
        if imageRequest != nil {
            imageRequest?.cancel()
        }
    }
    
    func loadImage(cell: MovieTableViewCell) {
        cell.pictureImageView.image = UIImage(named: "trakt-icon-red")
        if picture == nil {
//            print(AppDelegate.url_fanart + tmdb_id)
            
            let parameters: Parameters = [
                "api_key": AppDelegate.fanart_api_key
            ]
            
            cell.activityIndicator.startAnimating()
            
            Alamofire.request(AppDelegate.url_fanart + tmdb_id, method: .get, parameters: parameters).response { [weak self] response in
//                print(JSON(response.data))
                
                cell.activityIndicator.stopAnimating()
                
                guard let unwrappedSelf = self else { return }
                
                if response.error == nil && response.response?.statusCode == 200 {
                    let json = JSON(response.data!)
                    
                    let posterUrls = json["moviethumb"].arrayValue
                    var poster: JSON?
                    if !posterUrls.isEmpty {
                        poster = posterUrls.first(where: { (json) -> Bool in
                            return json["lang"].stringValue == NSLocalizedString("lang", comment: "") })
//                        print ("filterdData: \(poster)")
                    }
                    
                    var url: URL
                    if poster != nil {
                        url = URL(string: poster!["url"].stringValue)!
                        let data = try? Data(contentsOf: url)
                        if data != nil {
                            unwrappedSelf.picture = UIImage(data: data!)
                            cell.pictureImageView.image = unwrappedSelf.picture
                        }
                    }
                    else {
                        unwrappedSelf.picture = UIImage(named: "trakt-icon-red")
                        cell.pictureImageView.image = unwrappedSelf.picture
                    }
                }
            }
        }
        else {
            cell.pictureImageView.image = self.picture
        }
    }
}
