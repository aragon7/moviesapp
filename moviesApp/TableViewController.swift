//
//  TableViewController.swift
//  moviesApp
//
//  Created by Apple on 6/01/19.
//  Copyright © 2019 sergiodev. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class TableViewController: UITableViewController, UISearchControllerDelegate, UISearchResultsUpdating, UISearchBarDelegate {
    
    private var searchController = UISearchController(searchResultsController: nil)
    private var searchRequest: DataRequest?
    
    let headers: HTTPHeaders = [
        "Content-Type": "application/json",
        "trakt-api-key": AppDelegate.trakt_client_id,
        "trakt-api-version": "2"
    ]
    
    var movies = [Movie]()
    var currentPage = 1 //current page loaded
    private var shouldShowLoadingCell = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setSearchController()
        
//        refreshControl = UIRefreshControl()
//        refreshControl?.addTarget(self, action: #selector(refreshTable), for: .valueChanged)
//        refreshControl?.beginRefreshing()
        
        self.checkConnectivity()
    }
    
    func checkConnectivity () {
        if !NetworkReachabilityManager()!.isReachable {
            self.showAlert(title: "Error", message: NSLocalizedString("No Internet", comment: ""))
            return
        }
        self.loadMovies()
    }
    
    func showAlert(title: String, message: String)->Void {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Retry", style: .default, handler: { action in
            
            self.checkConnectivity()
        }))
        self.present(alert, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Private methods
    
    private func loadMovies(refresh: Bool = false) {
        if refresh {
            cleanTableView()
        }
        print("Loading page \(currentPage)")
        
        let parameters: Parameters = [
            "page": currentPage,
            "extended": "full"
        ]
        
        Alamofire.request(AppDelegate.url_movies, method: .get, parameters: parameters, headers: headers).response { [unowned self] response in
//            print(response.error)
//            print(JSON(response.data))
            
            if response.error == nil && response.response?.statusCode == 200 {
                let json = JSON(response.data!)
                
                for movieJson in json.array! {
                    let newMovie = Movie(json: movieJson)
                    self.movies.append(newMovie)
                }
                
                let pageCount = response.response?.allHeaderFields["x-pagination-page-count"] as! String
                self.shouldShowLoadingCell = self.currentPage < Int(pageCount)!
                
                self.refreshControl?.endRefreshing()
                self.tableView.reloadData()
            }
        }
    }
    
    private func querySearch() {
        print("Loading page \(currentPage)")
        
        let parameters: Parameters = [
            "query": searchController.searchBar.text!,
            "page": currentPage,
            "extended": "full"
        ]
        
        searchRequest = Alamofire.request(AppDelegate.url_search, method: .get, parameters: parameters, headers: headers).response { [unowned self] response in
//            print(response.error)
            print(JSON(response.data))
            
            if response.error == nil && response.response?.statusCode == 200 {
                
                let json = JSON(response.data!)
                
                for movieJson in json.array! {
                    let newMovie = Movie(json: movieJson["movie"])
                    newMovie.overview = movieJson["movie"]["overview"].stringValue
                    self.movies.append(newMovie)
                }
                
                let pageCount = response.response?.allHeaderFields["x-pagination-page-count"] as! String
                self.shouldShowLoadingCell = self.currentPage < Int(pageCount)!
                
                self.refreshControl?.endRefreshing()
                self.tableView.reloadData()
            }
        }
    }
    
    private func cleanTableView() {
        currentPage = 1
        self.movies.removeAll()
        self.tableView.reloadData()
    }
    
    private func setSearchController() {
        searchController.searchBar.delegate = self
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.keyboardType = UIKeyboardType.asciiCapable
        searchController.searchBar.placeholder = NSLocalizedString("Searchbar placeholder", comment: "")
        if #available(iOS 11.0, *) {
            navigationItem.searchController = searchController
            navigationItem.hidesSearchBarWhenScrolling = false
        } else {
            navigationItem.titleView = searchController.searchBar
        }
        definesPresentationContext = true
    }
    
    private func searchBarIsEmpty() -> Bool {
        return searchController.searchBar.text?.isEmpty ?? true
    }
    
    private func isSearching() -> Bool {
        return searchController.isActive && !searchBarIsEmpty()
    }
    
    //indicates if the row is a loadingCell
    private func isLoadingIndexPath(indexPath: IndexPath) -> Bool {
        guard shouldShowLoadingCell else {return false}
        return indexPath.row == self.movies.count
    }
    
    @objc
    private func refreshTable() {
        loadMovies(refresh: true)
    }
    
    // MARK: - Search Bar Delegate
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        loadMovies(refresh: true)
    }
    
    // MARK: - UISearchResultsUpdating Delegate
    
    func updateSearchResults(for searchController: UISearchController) {
        cleanTableView()
        
        if searchRequest != nil { //cancels previous search request
            searchRequest?.cancel()
        }
        refreshControl?.beginRefreshing()
        
        if searchBarIsEmpty() {
            return
        }
        
        querySearch()
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var count = movies.count
        return shouldShowLoadingCell ? count + 1 : count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        print("cellForRow: \(indexPath.row)")
        if isLoadingIndexPath(indexPath: indexPath) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "loadingCell", for: indexPath) as! LoadingCell
            cell.activityIndicator.startAnimating()
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "movieCell", for: indexPath) as! MovieTableViewCell
        let movie = movies[indexPath.row]
        
        cell.titleLabel.text = movie.title
        cell.ratingLabel.text = movie.rating.description + "%"
        cell.yearLabel.text = movie.year.description
        
        if isSearching() {
            cell.overviewLabel.text = movie.overview
            cell.pictureImageView.alpha = 0.5
        }
        else {
            cell.overviewLabel.text = ""
            cell.pictureImageView.alpha = 1.0
        }
        
        movie.loadImage(cell: cell)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        print("willDisplayRow: \(indexPath.row)")
        
        //fetch next page if loadingCell is reached
        guard isLoadingIndexPath(indexPath: indexPath) else {return}
        currentPage += 1
        if isSearching() {
            querySearch()
        }
        else {
            loadMovies()
        }
    }
    
    override func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        if searchController.isActive && searchController.searchBar.text == "" {
            self.searchController.isActive = false
            loadMovies(refresh: true)
        }
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
